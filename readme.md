# Laravel Morph Model
> The quick build morph models

Allows you to build file and image uploading.

## Installing / Getting started

```shell
composer install navinlab/laravel-morph-model
```

## Developing

To start developing:

```shell
git clone https://bitbucket.org/navinlab/laravel-morph-model.git
cd laravel-morph-model/
composer install
```

To use in Laravel project till developing, add into the project composer.json:
```json
  "repositories": [
    {
      "type": "path",
      "url": "<path>/laravel-package-starter",
      "options": {
        "symlink": true
      }
    }
  ],
```
Change the package composer.json version manually (should me grater that current):
```json
"version": "0.2.0",
```
And run composer update inside the project.

## Features

A list of morph models and migrations to perfom your development:
* **File**, **FileMigration** - upload file
* **Document**, **DocumentMigration** - upload document
* **Asset**, **AssetMigration** - upload asset (same as document)
* **Image**, **ImageMigration** - upload image
* **ThumbnailImage**, **ImageMigration** - upload image with thumbnails creation


## Contributing

As I use this for my own projects, I know this might not be the perfect approach
for all the projects out there. If you have any ideas, just
[open an issue][issues] and tell me what you think.

If you'd like to contribute, please fork the repository and make changes as
you'd like. Pull requests are warmly welcome.


## Licensing

This project is licensed under the MIT license.

## Documentation

#### Upload a file
Migration:
```php
use NavinLab\LaravelMorphModel\Migrations\FileMigration;

class CreateFilesTable extends FileMigration
{
    /**
     * @inheritdoc
     */
    protected function schemaCreateAdditional(\Illuminate\Database\Schema\Blueprint $table) {
        //add here additional fields, for example company_id with foreign key to companies
    }
}
```
Model:
```php
namespace App\Models\Morph;
use Illuminate\Support\Facades\Config;
use NavinLab\LaravelMorphModel\Models as Morph;

class File extends Morph\File
{
    /**
     * Get storage path of the file (based on configuration)
     * @return mixed
     */
    public function getStoragePathAttribute()
    {
        $property = $this->getMorphableId();
        return sprintf('%s/%d/%s.%s',
            Config::get('files.storage_prefix', 'files'),
            $this->$property,
            $this->hash,
            $this->extension
        );
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function user() {
        return parent::morphable();
    }

    /**
     *
     * @param Builder $query
     * @param User $user
     * @return mixed
     */
    public function scopeOfUser(Builder $query, User $user) {
        return parent::scopeOfMorphable($query, $user);
    }
}
```
Request (model has file attribute wich is UploadedFile istance): 
```php
class StoreRequest extends FormRequest
{
    //...
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'  => 'required|file|min:1|max:20480',
        ];
    }
}
```
Controller:
```php
    class UploadController 
    {
        public function store(StoreRequest $request) {
            $file = new File($request->all());
            $file->user()->associate(Auth::user());
            $file->save();
            
            return $file;
        }
        
        public function show($id, File $file) {
            return $file;
        }
        
        public function destroy($id, File $file) {
            $file->delete();
            
            return $file;
        }
    }
```

#### Upload a document/asset
Same as file, just use different base model class and migration. Different is the table name, assets and documents.

#### Upload an image
Here is just the model example

```php
namespace App\Models\Morph;
use Illuminate\Support\Facades\Config;
use NavinLab\LaravelMorphModel\Models as Morph;

class Image extends Morph\Image
{
    /**
     * Get storage path of the file (based on configuration)
     * @return mixed
     */
    public function getStoragePathAttribute()
    {
        $property = $this->getMorphableId();
        return sprintf('%s/%d/%s.%s',
            Config::get('images.storage_prefix', 'images'),
            $this->$property,
            $this->hash,
            $this->extension
        );
    }
    
    //...
}
```

#### Upload an image and create thumbnails with intervention/image package
We suggest to install the intervention/image package to create thumbnails
```shell
composer install intervention/image
```
Config (config/images.php):
```php
return [
    'storage_path' => 'images',
    'thumbnail_sizes' => [
        //key => [width, height]  
        'small' => [50, 50],
        'medium' => [150, 150],
        'large' => [400, 400],
    ],
];
```
Model:
```php
namespace App\Models\Morph;
use Illuminate\Support\Facades\Config;
use NavinLab\LaravelMorphModel\Models as Morph;

class ThumbnailImage extends Morph\ThumbnailImage
{
    public function getThumbnailSizesAttribute() {
        return Config::get('images.thumbnail_sizes', []);
    }

    public function getThumbnailStoragePath($prefix = '')
    {
        $property = $this->getMorphableId();
        return sprintf('%s/%d/%s-%s.%s',
            Config::get('images.storage_prefix', 'images'),
            $this->$property,
            $this->hash,
            $prefix,
            $this->extension
        );
    }
    
    //...
}
```

#### Upload an image and create thumbnails with custom implementation
Just use trait Thumbnails;
```php
namespace App\Models\Morph;
use Illuminate\Support\Facades\Config;
use NavinLab\LaravelMorphModel\Models as Morph;
use NavinLab\LaravelMorphModel\Traits\Thumbnails;

class ThumbnailImage extends Morph\Image
{
    use Thumbnails;
    
    public function getThumbnailSizesAttribute() {
        return Config::get('images.thumbnail_sizes', []);
    }
    
    public function getThumbnail($width, $height) {
        // TODO: Implement getThumbnail() method.
        // return (string) Image::make($this->file_source)->fit($width, $height)->stream();
    }

    public function getThumbnailStoragePath($prefix = 'origin') {
        $property = $this->getMorphableId();
        return sprintf('%s/%d/%s-%s.%s',
            Config::get('images.storage_prefix', 'images'),
            $this->$property,
            $this->hash,
            $prefix,
            $this->extension
        );
    }
    
    //...
}
```
#### Upload events
You can use uploaded event. 
```php
    File::uploaded(function (File $file) {
        //do logic here 
    });
```
You check an implementation in Thumbnails trait.
 
#### Storage disk customization
Just override tht getStorageDisk method on your model. By default it is null and filesystems.default will be used.
```php
class File extends Morph\File 
{
    //...
    protected function getStorageDisk() {
        return 'another';
    }
    //...
}
```
