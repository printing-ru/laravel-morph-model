<?php
namespace NavinLab\LaravelMorphModel\Migrations;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use NavinLab\LaravelMorphModel\Models\File;

/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 13:24
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */
abstract class FileMigration extends Migration
{
    /**
     * The table name
     *
     * @return mixed
     */
    protected function getTableName() {
        return 'files';
    }

    /**
     * The morphable field name
     *
     * @return string
     */
    protected function getMorphsFieldName() {
        return File::MORPHABLE;
    }
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->getTableName(), function (Blueprint $table) {
            $table->increments('id');
            //to store suffix
            $table->string('hash', 255);
            //to store file basename
            $table->string('basename', 255);
            //additional information
            $table->text('description')->nullable();
            //to store extension to manage file
            $table->string('extension', 10);
            //to show file size
            $table->integer('size')->unsigned();
            $table->string('mime_type', 255);

            $table->morphs($this->getMorphsFieldName());

            $table->timestamps();

            $this->schemaCreateAdditional($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->getTableName());
    }

    /**
     * Additional create schema commands
     *
     * @example
     * $table->integer('user_id')->unsigned();
     * $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
     *
     * @example
     * $table->softDeletes();
     *
     * @param $table
     * @return mixed
     */
    abstract protected function schemaCreateAdditional(Blueprint $table);
}