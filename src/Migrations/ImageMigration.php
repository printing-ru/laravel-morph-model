<?php
/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 13:36
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMorphModel\Migrations;

use NavinLab\LaravelMorphModel\Models\Image;

abstract class ImageMigration extends FileMigration
{
    /**
     * The table name
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'images';
    }

    /**
     * The morphable field name
     *
     * @return string
     */
    protected function getMorphsFieldName()
    {
        return Image::MORPHABLE;
    }
}