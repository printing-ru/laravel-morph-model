<?php
/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 13:37
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMorphModel\Migrations;

use NavinLab\LaravelMorphModel\Models\Asset;

abstract class AssetMigration extends FileMigration
{
    /**
     * The table name
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'assets';
    }

    /**
     * The morphable field name
     *
     * @return string
     */
    protected function getMorphsFieldName()
    {
        return Asset::MORPHABLE;
    }
}