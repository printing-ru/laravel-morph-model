<?php
/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 13:35
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMorphModel\Migrations;

use NavinLab\LaravelMorphModel\Models\Document;

abstract class DocumentMigration extends FileMigration
{
    /**
     * The table name
     *
     * @return mixed
     */
    protected function getTableName()
    {
        return 'documents';
    }

    /**
     * The morphable field name
     *
     * @return string
     */
    protected function getMorphsFieldName()
    {
        return Document::MORPHABLE;
    }
}