<?php
namespace NavinLab\LaravelMorphModel\Jobs\Image;

use Illuminate\Bus\Queueable;
use NavinLab\LaravelMorphModel\Models;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class Image implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    /**
     * @var Models\Image
     */
    protected $image;

    /**
     *
     * @param Image $image
     */
    public function __construct(Models\Image $image)
    {
        $this->image = $image;
    }
}