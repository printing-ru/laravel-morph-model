<?php
namespace NavinLab\LaravelMorphModel\Jobs\Image\Thumbnails;

use Illuminate\Support\Facades\Storage;
use NavinLab\LaravelMorphModel\Jobs\Image\Image;

class Deleted extends Image
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->image->thumbnail_sizes as $prefix => $dimension) {
            Storage::disk($this->image->getStorageDisk())->delete($this->image->getThumbnailStoragePath($prefix));
        }
    }
}
