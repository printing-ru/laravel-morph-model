<?php
namespace NavinLab\LaravelMorphModel\Jobs\Image\Thumbnails;

use Illuminate\Support\Facades\Storage;
use NavinLab\LaravelMorphModel\Jobs\Image\Image;

class Uploaded extends Image
{
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->image->thumbnail_sizes as $prefix => $dimension) {
            list($width, $height) = $dimension;

            Storage::disk($this->image->getStorageDisk())->put(
                $this->image->getThumbnailStoragePath($prefix),
                $this->image->getThumbnail($width, $height)
            );
        }
    }
}
