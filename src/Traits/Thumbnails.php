<?php
/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 08:44
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace NavinLab\LaravelMorphModel\Traits;


use NavinLab\LaravelMorphModel\Models\Image;
use NavinLab\LaravelMorphModel\Jobs;

trait Thumbnails
{
    public static function bootThumbnails()
    {
        static::uploaded(function(Image $image) {
            Image::getEventDispatcher()->dispatch(new Jobs\Image\Thumbnails\Uploaded($image));
        });

        //if switch to not softdeletes
        static::deleted(function(Image $image) {
            if (isset($image->forceDeleting) && !$image->forceDeleting) {
                return;
            }
            Image::getEventDispatcher()->dispatch(new Jobs\Image\Thumbnails\Deleted($image));
        });
    }

    /**
     * @return mixed
     */
    abstract public function getThumbnailSizesAttribute();
    abstract public function getThumbnail($width, $height);

    /**
     * Get storage path of the file thumbnail (based on configuration)
     *
     * @param string $prefix
     * @return mixed
     */
    abstract public function getThumbnailStoragePath($prefix = 'origin');

    /**
     * Get storage path of file (based on cdn features)
     * @return mixed
     */
    public function getStoragePathAttribute() {
        return $this->getThumbnailStoragePath();
    }

}