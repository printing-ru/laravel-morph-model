<?php

namespace NavinLab\LaravelMorphModel\Models;

abstract class Asset extends File
{
    /**
     * @inheritdoc
     */
    protected $table = 'assets';

    const MORPHABLE = 'assetable';

    /**
     * @return string
     */
    protected function getMorphable() {
        return self::MORPHABLE;
    }
}
