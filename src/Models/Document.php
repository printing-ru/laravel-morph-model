<?php

namespace NavinLab\LaravelMorphModel\Models;

abstract class Document extends File
{
    /**
     * @inheritdoc
     */
    protected $table = 'documents';

    const MORPHABLE = 'documentable';

    /**
     * @return string
     */
    protected function getMorphable() {
        return self::MORPHABLE;
    }
}
