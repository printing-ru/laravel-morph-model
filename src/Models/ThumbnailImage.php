<?php

namespace NavinLab\LaravelMorphModel\Models;

use Intervention\Image\Facades\Image as ImageManager;
use NavinLab\LaravelMorphModel\Traits\Thumbnails;

abstract class ThumbnailImage extends Image
{
    use Thumbnails;

    public function getThumbnail($width, $height) {
        return (string) ImageManager::make($this->file_source)->fit($width, $height)->stream();
    }
}
