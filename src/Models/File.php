<?php

namespace NavinLab\LaravelMorphModel\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

abstract class File extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'files';

    const MORPHABLE = 'fileable';

    /**
     * The default storage disk
     *
     * @return null
     */
    public function getStorageDisk() {
        return null;
    }

    /**
     * @return string
     */
    protected function getMorphable() {
        return self::MORPHABLE;
    }

    /**
     * The morphable id
     * @return string
     */
    protected final function getMorphableId() {
        return sprintf('%s_id', $this->getMorphable());
    }

    /**
     * The morphable type
     * @return string
     */
    protected final function getMorphableType() {
        return sprintf('%s_type', $this->getMorphable());
    }

    /**
     * Get storage path of the file (based on configuration)
     * @return mixed
     */
    abstract public function getStoragePathAttribute();

    /**
     * @var array
     */
    protected $fillable = ['description', 'name', 'file'];

    protected $casts = [
        'size' => 'integer',
    ];


    /**
     * Get the hidden attributes for the model.
     *
     * @return array
     */
    public function getHidden()
    {
        return array_merge(parent::getHidden(), [$this->getMorphable(), $this->getMorphableId(), $this->getMorphableType()]);
    }

    /**
     * Filed to hide
     *
     * @var array
     */
    protected $hidden = ['basename', 'hash', 'mime_type'];

    /**
     * @var array
     */
    protected $appends = ['name', 'url'];

    /**
     * Store file instance of uploaded file
     * @var UploadedFile
     */
    protected $fileInstance;

    /**
     * @param $fileInstance
     */
    public function setFileAttribute(UploadedFile $fileInstance) {
        $this->fileInstance = $fileInstance;
        //don't override if file already exists
        if (!$this->isDirty(['basename'])) {
            $this->basename     = $fileInstance->getClientOriginalName();
        } else {
            $this->basename     = sprintf('%s.%s', $this->name, $fileInstance->extension());
        }
        $this->extension    = strtolower($fileInstance->extension());
        $this->size         = $fileInstance->getSize();
        $this->mime_type    = $fileInstance->getMimeType();
        $this->hash         = $this->generateHash();
    }

    /**
     * @return string
     */
    protected function generateHash() {
        return Str::random(40);
    }

    /**
     * @return mixed
     */
    public function getFileAttribute() {
        return $this->fileInstance;
    }

    /**
     * @return mixed
     */
    public function getUrlAttribute() {
        return Storage::disk($this->getStorageDisk())->url($this->storage_path);
    }

    /**
     * @return string
     */
    public function getNameAttribute() {
        return pathinfo($this->basename, PATHINFO_FILENAME);
    }

    /**
     * @param $value
     */
    public function setNameAttribute($value) {
        $this->basename = sprintf('%s.%s', $value, $this->extension);
    }

    /**
     * @inheritdoc
     */
    protected $observables = ['uploaded'];
    /**
     * Register an uploaded model event with the dispatcher.
     *
     * @param  \Closure|string  $callback
     * @return void
     */
    public static function uploaded($callback)
    {
        static::registerModelEvent('uploaded', $callback);
    }

    /**
     * Boot listeners to store file in the storage
     */
    public static function boot()
    {
        parent::boot();
        $upload = function(File $file) {

            // get the file instance
            $uploadedFile = $file->file;
            // if file was set
            if (!$uploadedFile) return;

            //store file in the storage via resource
            Storage::disk($file->getStorageDisk())->put($file->storage_path, fopen($uploadedFile->path(), 'r'));
            //fire custom event
            $file->fireModelEvent('uploaded', false);
        };

        static::created($upload);
        static::updated($upload);

        //if switch to not softdeletes
        static::deleted(function(File $file) {
            //trick for the soft deletes
            if (isset($file->forceDeleting) && !$file->forceDeleting) {
                return;
            }
            Storage::disk($file->getStorageDisk())->delete($file->storage_path);
        });
    }

    /**
     * The source of the file
     * @var
     */
    protected $fileSourceCache;

    /**
     * @return mixed
     */
    public function getFileSourceAttribute() {
        if (!$this->fileSourceCache && $this->exists) {
            $this->fileSourceCache = Storage::disk($this->getStorageDisk())->get($this->storage_path);
        }
        return $this->fileSourceCache;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function morphable() {
        return $this->morphTo($this->getMorphable());
    }

    /**
     * @param $query
     * @param Model $model
     * @return mixed
     */
    public function scopeOfMorphable(Builder $query, Model $model) {
        return $query->where($this->getMorphableId(), $model->getKey())
                ->where($this->getMorphableType(), get_class($model));
    }
}
