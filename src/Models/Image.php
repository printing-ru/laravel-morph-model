<?php
namespace NavinLab\LaravelMorphModel\Models;

abstract class Image extends File
{
    /**
     * @inheritdoc
     */
    protected $table = 'images';

    const MORPHABLE = 'imageable';

    /**
     * @return string
     */
    protected function getMorphable() {
        return self::MORPHABLE;
    }
}
