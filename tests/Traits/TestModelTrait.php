<?php
/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 16:19
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Traits;


trait TestModelTrait
{
    /**
     * Get storage path of the file (based on configuration)
     * @return mixed
     */
    public function getStoragePathAttribute()
    {
        return 'storage_path';
    }

    public function getMorphableValue()
    {
        return parent::getMorphable();
    }

    public function getMorphableIdValue()
    {
        return parent::getMorphableId();
    }

    public function getMorphableTypeValue()
    {
        return parent::getMorphableType();
    }

    public function save(array $options = [])
    {
        if ($this->fireModelEvent('saving') === false) {
            return false;
        }

        if ($this->exists) {
            $saved = $this->isDirty() ?
                $this->performFakeUpdate() : true;
        }

        else {
            $saved = $this->performFakeInsert();
        }

        if ($saved) {
            $this->finishSave($options);
        }

        return true;
    }

    protected function performFakeInsert()
    {
        if ($this->fireModelEvent('creating') === false) {
            return false;
        }

        $this->exists = true;

        $this->wasRecentlyCreated = true;

        $this->fireModelEvent('created', false);

        return true;
    }

    protected function performFakeUpdate()
    {
        if ($this->fireModelEvent('updating') === false) {
            return false;
        }

        $dirty = $this->getDirty();

        if (count($dirty) > 0) {
            $this->fireModelEvent('updated', false);
        }

        return true;
    }

    protected function performDeleteOnModel() {
        return true;
    }
}