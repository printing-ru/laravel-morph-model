<?php
namespace Tests\Models;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\TestCase;

class FileTest extends TestCase
{
    protected $testModelClass = File::class;
    /**
     * @var UploadedFile
     */
    protected $file;

    public function setUp()
    {
        parent::setUp();
        $this->file = new UploadedFile(tempnam(sys_get_temp_dir(), 'upl'), 'file.eXt', null, null, UPLOAD_ERR_OK, true);
    }

    protected function getModel() {
        $testModelClass = $this->testModelClass;
        return new $testModelClass();
    }
    /**
     * Action test
     */
    public function testFileAttributes()
    {
        $model = $this->getModel();

        $model->file = $this->file;

        $this->assertEquals($this->file->getClientOriginalName(), $model->basename);
        $this->assertEquals(strtolower($this->file->extension()), $model->extension);
        $this->assertEquals($this->file->getSize(), $model->size);
        $this->assertEquals($this->file->getMimeType(), $model->mime_type);
        $this->assertNotEmpty($model->hash);

        $this->assertInstanceOf(UploadedFile::class, $model->file);

        Storage::shouldReceive('disk')->with($model->getStorageDisk())->andReturnSelf()
            ->shouldReceive('url')->with($model->storage_path)->andReturn('test-url');
        $this->assertEquals('test-url', $model->url);

        $this->assertEquals(pathinfo($this->file->getClientOriginalName(), PATHINFO_FILENAME), $model->name);

    }


    public function testSetNameAttribute() {
        $model = $this->getModel();
        //it should be before file
        $model->name = 'new_name';
        $model->file = $this->file;

        $this->assertEquals(sprintf('new_name.%s', strtolower($this->file->extension())), $model->basename);
        $this->assertEquals('new_name', $model->name);

        //and check that basename is correct (includes an extension)
        $this->assertEquals(sprintf('new_name.%s', $this->file->extension()), $model->basename);
    }

    public function testUpload() {
        $model = $this->getModel();
        $testModelClass = get_class($model);

        $testModelClass::setEventDispatcher($dispatcher = new \Illuminate\Events\Dispatcher());
        $testModelClass::boot();

        Storage::shouldReceive('disk')->with($model->getStorageDisk())->andReturnSelf()
            ->shouldReceive('put');

        $uploadedIsDispatched = false;
        $dispatcher->listen('eloquent.uploaded: ' . $testModelClass,
            function () use(&$uploadedIsDispatched) {
                $uploadedIsDispatched = true;
            }
        );


        $model->file = $this->file;

        $model->save();
        //check if uploaded event is dispatched
        $this->assertTrue($uploadedIsDispatched);

        //clean (isDirty should be false)
        $model->file = $this->file;
        //reset callback call
        $uploadedIsDispatched = false;
        //test update
        $model->save();

        //check if uploaded event is dispatched
        $this->assertTrue($uploadedIsDispatched);
    }

    public function testDelete() {
        $model = $this->getModel();

        $testModelClass = get_class($model);
        $testModelClass::setEventDispatcher($dispatcher = new \Illuminate\Events\Dispatcher());
        $testModelClass::boot();
        Storage::shouldReceive('disk')->with($model->getStorageDisk())->andReturnSelf()
            ->shouldReceive('delete')->with($model->storage_path)->andReturn(true);

        $model->exists = true;
        $this->assertTrue($model->delete());
    }

    public function testGetFileSourceAttributeOnExists() {
        //cached
        $model = $this->getModel();

        //emulate saved
        $model->exists = true;

        Storage::shouldReceive('disk')->with($model->getStorageDisk())->andReturnSelf()
            ->shouldReceive('get')->with($model->storage_path)->andReturn('test');

        $this->assertEquals('test', $model->file_source);
        //should be cached
        $this->assertEquals('test', $model->file_source);
    }

    public function testGetFileSourceAttributeOnNew() {
        //cached
        $model = $this->getModel();
        //emulate saved
        $model->exists = false;

        Storage::shouldReceive('disk')->with($model->getStorageDisk())->andReturnSelf()
            ->shouldReceive('get')->once();

        $this->assertNull($model->file_source);
    }

    public function testMorphable() {
        $model = $this->getModel();
        $testModelClass = get_class($model);
        $this->assertEquals($testModelClass::MORPHABLE, $model->getMorphableValue());
        $this->assertEquals($testModelClass::MORPHABLE . '_id', $model->getMorphableIdValue());
        $this->assertEquals($testModelClass::MORPHABLE . '_type', $model->getMorphableTypeValue());
    }
}