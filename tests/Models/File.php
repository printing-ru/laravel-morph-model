<?php
/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 11:01
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Models;

use NavinLab\LaravelMorphModel\Models;
use Tests\Traits\TestModelTrait;

class File extends Models\File
{
    use TestModelTrait;
}