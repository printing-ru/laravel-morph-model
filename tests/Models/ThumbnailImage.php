<?php
/**
 * laravel-morph-model.
 * Date: 11/05/17
 * Time: 10:42
 * @author Anton Pavlov <anton.pavlov.it@gmail.com>
 */

namespace Tests\Models;
use NavinLab\LaravelMorphModel\Models;

class ThumbnailImage extends Models\ThumbnailImage
{

    public function getThumbnailSizesAttribute()
    {
        return [
            'small' => [50, 50],
            'medium' => [100, 100],
            'large' => [200, 200]
        ];
    }

    public function getThumbnail($width, $height) {
        return sprintf('source-%d-%d', $width, $height);
    }

    public function getThumbnailStoragePath($prefix = '')
    {
        return $prefix;
    }
}