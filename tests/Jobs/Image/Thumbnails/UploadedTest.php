<?php
namespace Tests\Jobs\Image\Thumbnails;

use Illuminate\Support\Facades\Storage;
use NavinLab\LaravelMorphModel\Jobs\Image\Thumbnails\Uploaded;
use Tests\Models\ThumbnailImage;
use PHPUnit\Framework\TestCase;

class DestroyTest extends TestCase
{
    /**
     * Action test
     */
    public function testHandle()
    {
        $image = new ThumbnailImage();
        $sizes = $image->thumbnail_sizes;

        foreach ($sizes as $prefix => $dimension) {
            list($width, $height) = $dimension;
            Storage::shouldReceive('disk')->with($image->getStorageDisk())->andReturnSelf()
                ->shouldReceive('put')->with($prefix, $image->getThumbnail($width, $height));
        }

        (new Uploaded($image))->handle();
        $this->assertTrue(true);
    }
}