<?php
namespace Tests\Jobs\Image\Thumbnails;

use Illuminate\Support\Facades\Storage;
use NavinLab\LaravelMorphModel\Jobs\Image\Thumbnails\Deleted;
use Tests\Models\ThumbnailImage;
use PHPUnit\Framework\TestCase;

class DeletedTest extends TestCase
{
    /**
     * Action test
     */
    public function testHandle()
    {
        $image = new ThumbnailImage();
        $sizes = $image->thumbnail_sizes;
        foreach ($sizes as $prefix => $dimension) {
            Storage::shouldReceive('disk')->with($image->getStorageDisk())->andReturnSelf()
                ->shouldReceive('delete')->with($prefix);
        }

        (new Deleted($image))->handle();
        $this->assertTrue(true);
    }
}